use std::fs::File;
use std::io::Read;
use std::ops::Index;

const INPUT_PATH: &str = r"C:\Users\marku\Code\Rust\advent_2015\input\input02.txt";

struct Rectangle {
    length: i32,
    width: i32,
    height: i32,
}

impl Rectangle {
    fn new(length: i32, width: i32, height: i32) -> Rectangle {
        Rectangle {
            length,
            width,
            height,
        }
    }

    fn calculate_paper(&self) -> i32 {
        let l_and_w = self.length * self.width;
        let w_and_h = self.width * self.height;
        let h_and_l = self.height * self.length;
        let list = [l_and_w, w_and_h, h_and_l];
        let small = list.iter().min().unwrap();

        let result = (2 * l_and_w) + (2 * w_and_h) + (2 * h_and_l) + small;
        result
    }

    fn calculate_ribbon(&self) -> i32 {
        let bow = self.length * self.width * self.height; // Bow

        // List
        let mut temp_list = vec!(self.length, self.width, self.height);

        // Get smallest two sides.
        temp_list.sort();

        let small_first = temp_list[0] + temp_list[0];
        let small_second = temp_list[1] + temp_list[1];
        let result = bow + small_second + small_first;
        result
    }
}

pub fn calculate_wrapping_paper() -> i32 {
    match File::open(INPUT_PATH) {
        Ok(mut file) => {
            let mut buffer: String = String::new();
            file.read_to_string(&mut buffer).unwrap();
            work(&buffer)
        }
        Err(_error) => {
            println!("Error opening file");
            -1
        }
    }
}

fn work(input: &str) -> i32 {
    let lines: Vec<&str> = input.split('\n').collect();
    /* lines
    .iter()
    .fold(0, |x, y| x + string_to_rec(y).calculate_paper()) */
    lines
        .iter()
        .fold(0, |x, y| x + string_to_rec(y).calculate_ribbon())
}

fn string_to_rec(input: &str) -> Rectangle {
    let numbers: Vec<&str> = input.split('x').collect();
    Rectangle::new(
        numbers[0].trim_end().parse::<i32>().unwrap(),
        numbers[1].trim_end().parse::<i32>().unwrap(),
        numbers[2].trim_end().parse::<i32>().unwrap(),
    )
}
