// COMPLETE
use std::fs::File;
use std::io::Read;

const INPUT_PATH: &str = r"C:\Users\marku\Code\Rust\advent_2015\input\input01.txt";

pub fn count_parens() -> (i32, i32) {
    match File::open(INPUT_PATH) {
        Ok(mut file) => {
            let mut buffer: String = String::new();
            file.read_to_string(&mut buffer).unwrap();
            work(&buffer)
        }
        Err(error) => {
            println!("Error opening file");
            (-1, -1)
        }
    }
}

fn work(input: &str) -> (i32, i32) {
    let mut floor = 0;
    let mut position = 0;
    let mut position_flag = false;

    for x in input.chars() {
        if x == '(' {
            floor += 1;
            if !position_flag {
                position += 1
            }
        } else if x == ')' {
            floor -= 1;
            if !position_flag {
                position += 1
            }
        }

        println!("{} {}", floor, position);

        if !position_flag && floor == -1 {
            position_flag = true;
            println!("Locked")
        }
    }

    (floor, position)
}
